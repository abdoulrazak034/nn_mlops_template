import logging
import yaml
import importlib
import logging

# Setup logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def load_parameters(params_file):
    with open(params_file, "r") as f:
        params = yaml.safe_load(f)
    return params

# Assuming a file path like 'params.yml' for demonstration
params_file = 'params.yaml'  # Adjust this to your file path

# Load parameters
params = load_parameters(params_file)

# Access parameter values
project_name = params["project_name"]
project_dir = params["project_dir"]

print(f"Project Name: {project_name}")
print(f"Project Directory: {project_dir}")

logging.basicConfig(level=logging.INFO, format='[%(asctime)s]: %(message)s:')

def configure_dockerfile(filepath):
    """Configure Dockerfile for the project."""
    dockerfile_content = """
# Utilisez l'image de base officielle Python 3.8
FROM python:3.8-slim

# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez les fichiers de dépendances
COPY requirements.txt .

# Installez les dépendances du projet
RUN pip install --no-cache-dir -r requirements.txt

# Copiez le reste des fichiers du projet dans le conteneur
COPY . .

# Commande pour exécuter l'application
CMD ["python", "./main.py"]
"""
    try:
        with open(filepath, "w") as f:
            f.write(dockerfile_content)
        logging.info(f"Dockerfile configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring Dockerfile in {filepath}: {e}")

def configure_dockerignore(filepath):
    """Configure .dockerignore for the project."""
    dockerignore_content = """
__pycache__/
*.pyc
*.pyo
*.pyd
.env
"""
    try:
        with open(filepath, "w") as f:
            f.write(dockerignore_content)
        logging.info(f".dockerignore configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring .dockerignore in {filepath}: {e}")

def configure_docker_compose(filepath):
    """Configure docker-compose.yml for the project."""
    docker_compose_content = """
version: '3.8'
services:
  ml_project:
    build: .
    command: python ./main.py
    volumes:
      - .:/app
    ports:
      - "8000:8000"
    environment:
      - ENV=development
"""
    try:
        with open(filepath, "w") as f:
            f.write(docker_compose_content)
        logging.info(f"docker-compose.yml configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring docker-compose.yml in {filepath}: {e}")

def configure_require(filepath):
    """Configure project setup in the specified file."""
    try:
        with open(filepath, "w") as f:
            f.write("""
mlflow==2.12.1
dvc==3.50.0
dvc-ssh==4.1.1
scikit-learn
pandas
numpy
jupyterlab
seaborn
pandas 
gdown
notebook
matplotlib==3.8.4
python-box==6.0.2
pyYAML==6.0.1
tqdm==4.66.2
ensure==1.0.2
joblib
types-PyYAML
scipy
Flask
Flask-Cors
-e .

""")
        logging.info(f"setup configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring setup in {filepath}: {e}")

def configure_project_env(filepath):
    """Configure project setup in the specified file."""
    try:
        with open(filepath, "w") as f:
            f.write("""
#!/bin/bash

# Ce script configure l'environnement pour un projet Python, crée un environnement virtuel, et initialise DVC
# Assurez-vous que le script est exécutable
chmod +x setup_project.sh

echo "Setting up the Python project environment..."

# Créer un environnement virtuel nommé 'env'
echo "Creating virtual environment 'env'..."
python3 -m venv env

# Activer l'environnement virtuel
echo "Activating the virtual environment..."
source env/bin/activate

# Installer les dépendances à partir de requirements.txt
echo "Installing dependencies from requirements.txt..."
pip install -r requirements.txt

# Initialiser DVC dans le répertoire courant
echo "Initializing DVC..."
dvc init

# Reproduire les étapes du pipeline DVC
echo "Reproducing DVC pipeline..."
dvc repro

# Afficher le graphe de dépendances du pipeline DVC
echo "Displaying DVC pipeline DAG..."
dvc dag

echo "Setup completed successfully."

# L'utilisateur doit exécuter manuellement 'source env/bin/activate' pour réactiver l'environnement virtuel dans de nouvelles sessions shell.
echo "Run 'source env/bin/activate' to reactivate the virtual environment in new shell sessions."

""")
        logging.info(f"setup configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring setup in {filepath}: {e}")

def configure_setup(filepath, version, author, author_email, description, src_repo, package_dir='src'):
    """
    Configure setup in the specified file with customizable parameters.

    Args:
        filepath (str): The path to the setup file to be written.
        name (str): The name of the package.
        version (str): The package version.
        author (str): The name of the author.
        author_email (str): The email of the author.
        description (str): A brief description of the package.
        src_repo (str): The name of the source repository.
        package_dir (str, optional): The package directory. Defaults to 'src'.
    """
    try:
        with open(filepath, "w") as f:
            f.write(f"""
import setuptools

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()
    
__version__ = "{version}"
REPO_NAME = "{src_repo}"
AUTHOR_USER_NAME = "{author}"
AUTHOR_EMAIL = "{author_email}"

setuptools.setup(
    name= REPO_NAME,
    version=__version__,
    author=AUTHOR_USER_NAME,
    author_email=AUTHOR_EMAIL,
    description="{description}",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url=f"git@gitlab.cotral.com:/{{AUTHOR_USER_NAME}}/{{REPO_NAME}}",
    project_urls={{
        "Bug Tracker": f"git@gitlab.cotral.com:/{{AUTHOR_USER_NAME}}/{{REPO_NAME}}/issues",
    }},
    package_dir={{"": "{package_dir}"}},
    packages=setuptools.find_packages(where="{package_dir}")
)
            """)
        logging.info(f"Setup configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring setup in {filepath}: {e}")

def configure_logging(filepath):
    """Configure logging in the specified file."""
    try:
        with open(filepath, "w") as f:
            f.write("""
import os
import sys
import logging

logging_str = "[%(asctime)s: %(levelname)s: %(module)s: %(message)s]"

log_dir = "logs"
log_filepath = os.path.join(log_dir, "running_logs.log")
os.makedirs(log_dir, exist_ok=True)

logging.basicConfig(
    level=logging.INFO,
    format=logging_str,
    handlers=[
        logging.FileHandler(log_filepath),
        logging.StreamHandler(sys.stdout)
    ]
)

logger = logging.getLogger("mlProjectLogger")
                    """)
        logging.info(f"Logging configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring logging in {filepath}: {e}")


def configure_main(filepath, project_name):
    f"""Configure the main script in the specified file."""
    try:
        with open(filepath, "w") as f:
            f.write(f"""
import json
import logging
import importlib
from {project_name} import logger
from {project_name}.utils import time_keeper

# Setup logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from pprint import pprint


timer = time_keeper.TimeKeeper()
timer.add_stat_op(time_keeper.Average())
timer.add_stat_op(time_keeper.StandardDeviation())
timer.add_stat_op(time_keeper.NbElt())
timer.add_stat_op(time_keeper.Total())

def run_stage(stage_name, stage_function):
    try:
        logger.info(f">>>>>> stage {{stage_name}} started <<<<<<")
        # Execute the stage function
        stage_function()
        logger.info(f">>>>>> stage {{stage_name}} completed <<<<<< x==========x")
    except Exception as e:
        logger.exception(e)
        raise e

# Define stage functions
def data_ingestion_stage():
    pass  # Implement your data ingestion logic here

def prepare_base_model_stage():
    pass  # Implement your prepare base model logic here

def training_stage():
    pass  # Implement your training logic here

def evaluation_stage():
    pass  # Implement your evaluation logic here

if __name__ == "__main__":
    # Run stages
    timer.start("data_ingestion_stage")
    run_stage("c", data_ingestion_stage)
    timer.stop("data_ingestion_stage")
    timer.start("Prepare base model")
    run_stage("Prepare base model", prepare_base_model_stage)
    timer.stop("Prepare base model")
    timer.start("Training")
    run_stage("Training", training_stage)
    timer.stop("Training")
    timer.start("Evaluation")
    run_stage("Evaluation", evaluation_stage)
    timer.stop("Evaluation")    
    pprint(timer.get_statistics()) # Print the statistics
    with open("logs/Timer.json", "w") as out_dset:
        json.dump(timer.get_statistics(), out_dset, indent=6)
    logger.info("Timer.json file created")  # Log a message to indicate that the Timer.json file has been created
""")
        logging.info(f"Main script configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring main script in {filepath}: {e}")


def configure_readme(filepath):
    """Configure the main script in the specified file."""
    try:
        with open(filepath, "w") as f:
            f.write("""
# Your Project Title

## Overview
This project is designed to implement a robust MLOps pipeline integrating various technologies like MLflow for experiment tracking, DVC for data and model versioning, Docker for containerization, and more. The project aims to demonstrate an end-to-end machine learning lifecycle from data ingestion, training, evaluation, and deployment, focusing on reproducibility and scalability.

## Technologies Used
- **MLflow**: For experiment tracking and model registry.
- **DVC**: For data and model versioning.
- **Docker**: For creating and managing project containers.
- **Python**: The primary programming language used.
- **Jupyter Notebook**: For research and trial experiments.
``

## Setup Instructions
To get started with this project, follow these steps:

```bash
# Clone the repository
git clone <repository_url>

# Navigate to the project directory
cd <project_name>

# Run the setup script to set up the environment
./setup_project.sh

# Alternatively, manually set up the environment
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# Initialize DVC
dvc init
dvc pull
```
# Running the Project
You can run the project by executing the `main.py` script. 
Ensure to configure the `config/config.yaml` and params.yaml as per your requirements before running.

For detailed instructions on using Docker and Docker Compose, refer to the `Dockerfile` and `docker-compose.yml`.

# Documentation
For comprehensive guides and API usage, see the documentation in the `docs/` directory. Start with docs/index.md.

# Contributing
Contributions are welcome! Please read the contribution guidelines in CONTRIBUTING.md (create this file if it doesn't exist) for more information on how to contribute.

# License
This project is licensed under the [LICENSE] - insert your license here.

                    """)
        logging.info(f"Main script configuration added to: {filepath}")
    except OSError as e:
        logging.error(f"Error configuring main script in {filepath}: {e}")



def configure_gitignore(filepath, project_name):
    """Configure the main script in the specified file."""
    try:
        with open(filepath, "w") as f:
            f.write(f"""
# Exclure les fichiers de data
data/
# Exclure les fichiers de logs
logs/
# Exclure les fichiers de logs
env
# Répertoire MLflow ajouté au .gitignore
mlruns/
artifacts/
Le répertoire {project_name}.egg-info contient des métadonnées sur le package Python {project_name}. 


src/{project_name}/__pycache__/
src/{project_name}/components/__pycache__/
src/{project_name}/components/dataset/__pycache__/
src/{project_name}/config/__pycache__/
src/{project_name}/constants/__pycache__/
src/{project_name}/entity/__pycache__/
src/{project_name}/utils/__pycache__/
src/{project_name}/components/preprocessing/TF/__pycache__/
src/{project_name}/components/trainer/__pycache__
src/{project_name}/components/model/__pycache__/
src/{project_name}/components/data_injection/__pycache__/
src/{project_name}/components/transforms/__pycache__/
src/{project_name}/pipeline/__pycache__/
src/{project_name}.egg-info/
tests/audio
docs/_build
docs/_static
                """)
        logger.info(f"Gitignore configuration added to: {filepath}")
    except OSError as e:
        logger.error(f"Error configuring main script in {filepath}: {e}")
        

def configure_params_yaml(filepath):
    """Enhanced parameters configuration with detailed training and system settings."""
    try:
        with open(filepath, "w") as f:
            yaml.dump({
                "train": {
                    "seed": 1234,
                    "num_gpus": 0,
                    "rep_discriminator": 2,
                    "discriminator_train_start_steps": 1500000,
                    "postnet_start_steps": 800000,
                    "discriminator_train_start_epoch": 300,
                    "postnet_start_epoch": 200,
                    "num_workers": 4,
                    "batch_size": 1,
                    "optimizer": "adam",
                    "training_epochs": 500,
                    "stdout_interval": 10000,
                    "fine_tuning": False,
                    "adam": {
                        "lr_decay": 0.99
                    },
                    "adamG": {
                        "lr": 0.001,
                        "beta1": 0.5,
                        "beta2": 0.9,
                        "lr_decay": 0.999,
                        "lr_postnet": 0.0001,
                        "lr_disc": 0.00001
                    },
                    "adamD": {
                        "lr": 0.001,
                        "beta1": 0.5,
                        "beta2": 0.9
                    }
                },
                "noise": {
                    "air_conditioner": 5,
                    "car_horn": 1,
                    "children_playing": 1,
                    "dog_bark": 1,
                    "drilling": 5,
                    "engine_idling": 5,
                    "gun_shot": 1,
                    "jackhammer": 5,
                    "siren": 1,
                    "street_music": 1
                },
                "dist": {
                    "dist_backend": "nccl",
                    "dist_url": "tcp://localhost:54321",
                    "world_size": 1,
                    "rank": 0
                },
                "audio": {
                    "n_mel_channels": 80,
                    "duration": 10,
                    "segment_length": 160000,
                    "pad_short": 2000,
                    "filter_length": 1024,
                    "hop_length": 256,
                    "win_length": 1024,
                    "sampling_rate": 16000,
                    "mel_fmin": 0.0,
                    "mel_fmax": 8000.0,
                    "fmax_for_loss": None
                },
                "logs": {
                    "summary_interval": 20,
                    "validation_interval": 2,
                    "evaluation_interval": 4000,
                    "save_interval": 1000,
                    "chkpt_dir": "/home/abdoul/git/mla_nn/hifigan_denoiser/monitoring/chkpt_mla_hifigan/200h_auto_cv/",
                    "log_dir": "logs",
                    "old_events_file": "",
                    "test_metrics": "/home/abdoul/git/mla_nn/hifigan_denoiser/track_metrics/"
                }
            }, f, default_flow_style=False)
        logger.info(f"Params configuration added to: {filepath}")
    except OSError as e:
        logger.error(f"Error enhancing parameters in {filepath}: {e}")




def configure_dvc_yaml(filepath):
    """Enhance the DVC pipeline configuration to align with detailed project settings."""
    try:
        with open(filepath, "w") as f:
            f.write("""
stages:
  data_ingestion:
    cmd: python src/hifigan_mlops/pipeline/stage_data_ingestion.py
    deps:
      - src/hifigan_mlops/pipeline/stage_data_ingestion.py
      - data/raw
    outs:
      - data/processed/data_ingested.dvc

  prepare_base_model:
    cmd: python src/hifigan_mlops/pipeline/stage_prepare_base_model.py
    deps:
      - src/hifigan_mlops/pipeline/stage_prepare_base_model.py
    outs:
      - models/base_model.dvc

  model_training:
    cmd: python src/hifigan_mlops/pipeline/stage_model_trainer.py
    deps:
      - src/hifigan_mlops/pipeline/stage_model_trainer.py
      - data/processed/data_ingested.dvc
    outs:
      - models/trained_model.dvc

  model_evaluation:
    cmd: python src/hifigan_mlops/pipeline/stage_model_evaluation.py
    deps:
      - src/hifigan_mlops/pipeline/stage_model_evaluation.py
      - models/trained_model.dvc
    outs:
      - results/evaluation_results.dvc

            """)
        logger.info("DVC pipeline stages defined for data ingestion, model training, and evaluation.")
    except OSError as e:
        logger.error(f"Error enhancing DVC pipeline in {filepath}: {e}")


def configure_gitlab_ci(filepath):
    """Enhance the DVC pipeline configuration to align with detailed project settings."""
    try:
        with open(filepath, "w") as f:
            f.write("""
stages:
  - deploy
  - train_and_report
deploy_job:
  stage: deploy
  when: always
  image: iterativeai/cml:0-dvc2-base1
  script:
    - cml runner
        --coud aws
        --cloud-region us-west
        --cloud-type g3.4xlarge
        --cloud-hdd-size 64
        --labels=cml-runner_gpu

train_and_report:
  image: iterativeai/cml:0-dvc2-base1
  stage: train_and_report
  script:
    - pip install -r requirements.txt
    - dvc pull data --run-cache
    - dvc repro

    # Create CML report
    - echo "## Metrics: workflow vs. main" >> report.md
    - git fetch --depth=1 origin main:main
    - dvc metrics diff --show-md main >> report.md

    - echo "## Plots" >> report.md
    - echo "### Training loss function diff" >> report.md
    - dvc plots diff --target loss.csv --show-vega main > vega.json
    - vl2png vega.json > plot.png
    - echo '![](./plot.png "Training Loss")' >> report.md

    - cml comment create report.md

            """)
        logger.info("GitLab CI configuration added to manage continuous integration and deployment.")
    except OSError as e:
        logger.error(f"Error enhancing DVC pipeline in {filepath}: {e}")
        
        

def configure_mlflow_config(filepath):
    """Enhance the DVC pipeline configuration to align with detailed project settings."""
    try:
        with open(filepath, "w") as f:
            f.write("""
mlflow:
  tracking:
    # URI du serveur de suivi MLflow
    uri: http://localhost:5000

    # Nom de l'expérience sous laquelle enregistrer les runs
    experiment_name: MonProjetExperiment

    # Authentification, si nécessaire
    auth:
      username: monUtilisateur
      password: monMotDePasse
      token: monTokenDeSecurite

  # Configuration des emplacements d'artefacts
  artifacts:
    # Emplacement par défaut pour stocker les artefacts
    location: s3://mon-bucket-mlflow/artefacts

    # Options de stockage spécifiques (AWS S3, Google Cloud Storage, Azure Blob Storage, etc.)
    storage_options:
      aws:
        access_key_id: monAccessKeyId
        secret_access_key: monSecretAccessKey
      gcp:
        project: monProjetGCP
        credentials: chemin/vers/mes/credentials/gcp.json
      azure:
        connection_string: MaConnectionStringAzure

  # Paramètres pour le suivi et le logging des modèles
  model_logging:
    # Spécifie si les modèles doivent être loggés avec leur signature d'entrée
    log_input_examples: true
    # Spécifie si les modèles doivent être loggés avec leur schéma
    log_model_signature: true

  # Configuration pour la gestion avancée et les fonctionnalités de MLflow
  advanced:
    # Activer ou désactiver la fonctionnalité de tracking d'URI dynamique
    dynamic_tracking_uri: false
    # Configuration pour des fonctionnalités expérimentales
    experimental:
      feature_x: true

            """)
        logger.info("MLflow configuration added to manage experiment tracking and model registry.")
    except OSError as e:
        logger.error(f"Error enhancing DVC pipeline in {filepath}: {e}")
        
        
        
def configure_constant(filepath):
    """Enhance the DVC pipeline configuration to align with detailed project settings."""
    try:
        with open(filepath, "w") as f:
            f.write("""
from pathlib import Path

CONFIG_FILE_PATH = Path("config/config.yaml")
PARAMS_FILE_PATH = Path("params.yaml")
MLFOW_PARAMS_FILE_PATH = Path("config/mlflow_config.yaml")
            """)
        logger.info("Constant configuration added to manage file paths.")
    except OSError as e:
        logger.error(f"Error enhancing DVC pipeline in {filepath}: {e}")
        
        
def configure_common(filepath):
    """Enhance the DVC pipeline configuration to align with detailed project settings."""
    try:
        with open(filepath, "w") as f:
            f.write("""
import os
from box.exceptions import BoxValueError
import yaml
import json
import joblib
from ensure import ensure_annotations
from box import ConfigBox
from pathlib import Path
from typing import Any
import base64



@ensure_annotations
def read_yaml(path_to_yaml: Path) -> ConfigBox:
    try:
        with open(path_to_yaml) as yaml_file:
            content = yaml.safe_load(yaml_file)
            logger.info(f"yaml file: {path_to_yaml} loaded successfully")
            return ConfigBox(content)
    except BoxValueError:
        raise ValueError("yaml file is empty")
    except Exception as e:
        raise e
    


@ensure_annotations
def create_directories(path_to_directories: list, verbose=True):
    for path in path_to_directories:
        os.makedirs(path, exist_ok=True)
        if verbose:
            logger.info(f"created directory at: {path}")


@ensure_annotations
def save_json(path: Path, data: dict):

    with open(path, "w") as f:
        json.dump(data, f, indent=4)

    logger.info(f"json file saved at: {path}")




@ensure_annotations
def load_json(path: Path) -> ConfigBox:

    with open(path) as f:
        content = json.load(f)

    logger.info(f"json file loaded succesfully from: {path}")
    return ConfigBox(content)


@ensure_annotations
def save_bin(data: Any, path: Path):

    joblib.dump(value=data, filename=path)
    logger.info(f"binary file saved at: {path}")


@ensure_annotations
def load_bin(path: Path) -> Any:

    data = joblib.load(path)
    logger.info(f"binary file loaded from: {path}")
    return data

@ensure_annotations
def get_size(path: Path) -> str:

    size_in_kb = round(os.path.getsize(path)/1024)
    return f"~ {size_in_kb} KB"

@ensure_annotations
def merge_audio_files(file_paths, output_file):
    try:
        audio_data = [librosa.load(file_path)[0] for file_path in file_paths]
        merged_audio = np.concatenate(audio_data)
        librosa.output.write_wav(output_file, merged_audio, sr=44100)  # Adjust sample rate as needed
        print(f"Merged audio saved to {output_file}")
    except Exception as e:
        print(f"Error merging audio files: {e}")

@ensure_annotations
def get_duration(self, file_name):
    try:
        duration_seconds = librosa.get_duration(path=file_name)
        duration_hours = duration_seconds / 3600  # Conversion en heures
        return duration_hours
    except Exception as e:
        print(f"Error processing {file_name}: {e}")
        return 0  # Si une erreur se produit, on renvoie 0 pour la durée


@ensure_annotations        
def get_audio_duration(self, file_paths):
    durations = []
    for file_path in file_paths:
        info = librosa.get_duration(filename=file_path)
        durations.append(info)
    return durations

@ensure_annotations
def get_files_for_duration(self, file_list, target_duration_hours):
        # First, parallelize the retrieval of durations for all files
        durations = Parallel(n_jobs=-1)(delayed(self.get_duration)(file_name) for file_name in tqdm(file_list, desc="Processing files"))

        # Sequentially iterate through the durations to accumulate until the target is reached
        files_covering_duration = []
        current_duration = 0.0

        for file_name, duration in zip(file_list, durations):
            if current_duration + duration <= target_duration_hours:
                files_covering_duration.append(file_name)
                current_duration += duration
            else:
                break  # Exit the loop if the target duration is exceeded

        return files_covering_duration

            """)
        logger.info("Common functions added for utility operations.")
    except OSError as e:
        logger.error(f"Error enhancing DVC pipeline in {filepath}: {e}")
        

def configure_time_keeper(filepath):
    """Enhance the DVC pipeline configuration to align with detailed project settings."""
    try:
        with open(filepath, "w") as f:
            f.write("""
import time
import numpy as np


class TimeKeeper:
    def __init__(self):
        self.begins = {}
        self.ends = {}
        self.stat_op = []

    def start(self, name):
        if name not in self.begins:
            self.begins[name] = []
        begin = time.perf_counter()
        self.begins[name].append(begin)

    def stop(self, name):
        end = time.perf_counter()
        if name not in self.ends:
            self.ends[name] = []
        self.ends[name].append(end)

    def add_stat_op(self, stat_op):
        self.stat_op.append(stat_op)

    def _compute_times(self):
        self.times = {}
        for name in self.begins:
            self.times[name] = []
            for b, e in zip(self.begins[name], self.ends[name]):
                self.times[name].append(e - b)

    def get_statistics(self):
        self._compute_times()
        stats = {}
        for time_name in self.begins:
            stats[time_name] = {}
            for op in self.stat_op:
                stats[time_name][op.name()] = op.perform(self.times[time_name])

        return stats


class Average:
    def name(self):
        return "average"

    def perform(self, val_array):
        return np.average(val_array)


class StandardDeviation:
    def name(self):
        return "std"

    def perform(self, val_array):
        return np.std(val_array)


class Total:
    def name(self):
        return "total"

    def perform(self, val_array):
        return np.sum(val_array)


class NbElt:
    def name(self):
        return "Nb elt"

    def perform(self, val_array):
        return len(val_array)


            """)
        logger.info("TimeKeeper class added to track time performance.")
    except OSError as e:
        logger.error(f"Error enhancing DVC pipeline in {filepath}: {e}")