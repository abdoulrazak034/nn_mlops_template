import os
from pathlib import Path
import logging
from config_functions import *
from utils import *

params = load_parameters("params.yaml")
project_name = params["project_name"]
project_dir = params["project_dir"]
name= params["project_dir"]
version= params["version"]
author= params["author"]
author_email= params["author_email"]
description= params["description"]
src_repo= params["src_repo"]
package_dir= params["package_dir"]

list_of_files = [
    f"{project_dir}{project_name}/.github/workflows/.gitkeep",
    f"{project_dir}{project_name}/src/{project_name}/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/components/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/components/dataset/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/components/dataset/data_ingestion.py",
    f"{project_dir}{project_name}/src/{project_name}/components/dataset/query_mla_sound.py",
    f"{project_dir}{project_name}/src/{project_name}/components/dataset/query_common_voice_sound.py",
    f"{project_dir}{project_name}/src/{project_name}/components/dataset/query_urban_sound.py",
    f"{project_dir}{project_name}/src/{project_name}/components/model/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/components/model/prepare_base_model.py",
    f"{project_dir}{project_name}/src/{project_name}/components/model/model_evaluation.py",
    f"{project_dir}{project_name}/src/{project_name}/components/trainer/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/components/trainer/model_trainer.py",
    f"{project_dir}{project_name}/src/{project_name}/components/preprocessing/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/utils/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/utils/common.py",
    f"{project_dir}{project_name}/src/{project_name}/utils/time_keeper.py",
    f"{project_dir}{project_name}/src/{project_name}/config/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/config/configuration.py",
    f"{project_dir}{project_name}/src/{project_name}/pipeline/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/pipeline/prediction.py",
    f"{project_dir}{project_name}/src/{project_name}/pipeline/stage_data_ingestion.py",
    f"{project_dir}{project_name}/src/{project_name}/pipeline/stage_prepare_data.py",
    f"{project_dir}{project_name}/src/{project_name}/pipeline/stage_prepare_base_model.py",
    f"{project_dir}{project_name}/src/{project_name}/pipeline/stage_model_trainer.py",
    f"{project_dir}{project_name}/src/{project_name}/pipeline/stage_model_evaluation.py",
    f"{project_dir}{project_name}/src/{project_name}/entity/__init__.py",
    f"{project_dir}{project_name}/src/{project_name}/entity/config_entity.py",
    f"{project_dir}{project_name}/src/{project_name}/constants/__init__.py",
    f"{project_dir}{project_name}/tests/__init__.py",
    f"{project_dir}{project_name}/tests/test_data_ingestion.py",
    f"{project_dir}{project_name}/tests/test_prepare_base_model.py",
    f"{project_dir}{project_name}/tests/test_model_trainer.py",
    f"{project_dir}{project_name}/tests/test_model_evaluation.py",
    f"{project_dir}{project_name}/tests/test_common_utils.py",
    f"{project_dir}{project_name}/tests/test_configuration.py",
    f"{project_dir}{project_name}/tests/test_pipeline_data_ingestion.py",
    f"{project_dir}{project_name}/tests/test_pipeline_prepare_base_model.py",
    f"{project_dir}{project_name}/tests/test_pipeline_model_training.py",
    f"{project_dir}{project_name}/tests/test_pipeline_model_evaluation.py",
    f"{project_dir}{project_name}/tests/test_end_to_end.py",
    f"{project_dir}{project_name}/config/config.yaml",
    f"{project_dir}{project_name}/config/mlflow_config.yaml",
    f"{project_dir}{project_name}/dvc.yaml",
    f"{project_dir}{project_name}/.gitignore",
    f"{project_dir}{project_name}/requirements.txt",
    f"{project_dir}{project_name}/setup.py",
    f"{project_dir}{project_name}/main.py",
    f"{project_dir}{project_name}/deploy.py",
    f"{project_dir}{project_name}/reports",
    f"{project_dir}{project_name}/reports/figures",
    f"{project_dir}{project_name}/setup_project.sh",
    f"{project_dir}{project_name}/research/trials.ipynb",
    f"{project_dir}{project_name}/templates/index.html",
    f"{project_dir}{project_name}/docs/conf.py",
    f"{project_dir}{project_name}/docs/index.md",
    f"{project_dir}{project_name}/docs/{project_name}_architecture.md",
    f"{project_dir}{project_name}/docs/{project_name}_api_usage.md",
    f"{project_dir}{project_name}/docs/quick_start.md",
    f"{project_dir}{project_name}/Dockerfile",
    f"{project_dir}{project_name}/.dockerignore",
    f"{project_dir}{project_name}/docker-compose.yml",
    f"{project_dir}{project_name}/README.md",
    f"{project_dir}{project_name}/.gitlab-ci.yml",
    f"{project_dir}{project_name}/data/raw/mla_data.json",
    f"{project_dir}{project_name}/data/raw/cv_data.json",
    f"{project_dir}{project_name}/data/raw/noise_data.json",
    f"{project_dir}{project_name}/data/raw/fixed_bdd_clean_test.json",
    f"{project_dir}{project_name}/data/raw/fixed_bdd_noise_test.json",
]


if os.path.exists(os.path.join(project_dir,project_name)):
    logging.info(f"Project directory '{project_name}' already exists.")
else:
    logging.info(f"Created project directory '{project_name}'.")
    for filepath in list_of_files:
        filepath = Path(filepath)
        filedir, filename = filepath.parent, filepath.name
        print(f'filedir: {filedir}, filename: {filename}')
        create_directory_if_not_exists(filedir)

        if not filepath.exists():
            if filepath == Path(f"{project_dir}{project_name}/setup.py"):
                configure_setup(filepath, version, author, author_email, description, src_repo, package_dir='src')
            elif filepath == Path(f"{project_dir}{project_name}/setup_project.sh"):
                configure_project_env(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/requirements.txt"):
                configure_require(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/Dockerfile"):
                configure_dockerfile(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/.dockerignore"):
                configure_dockerignore(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/docker-compose.yml"):
                configure_docker_compose(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/src/{project_name}/__init__.py"):
                configure_logging(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/main.py"):
                configure_main(filepath, project_name)
            elif filepath == Path(f"{project_dir}{project_name}/README.md"):
                configure_readme(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/.gitignore"):
                configure_gitignore(filepath, project_name)
            elif filepath == Path(f"{project_dir}{project_name}/dvc.yaml"):
                configure_dvc_yaml(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/.gitlab-ci.yml"):
                configure_gitlab_ci(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/config/mlflow_config.yaml"):
                configure_mlflow_config(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/config/config.yaml"):
                configure_params_yaml(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/src/{project_name}/constants/__init__.py"):
                configure_constant(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/src/{project_name}/utils/common.py"):
                configure_common(filepath)
            elif filepath == Path(f"{project_dir}{project_name}/src/{project_name}/utils/time_keeper.py"):
                configure_time_keeper(filepath)
            else:
                create_empty_file(filepath)
        else:
            logging.info(f"{filename} already exists.")
