import os
from pathlib import Path
import logging

logging.basicConfig(level=logging.INFO, format='[%(asctime)s]: %(message)s:')

def create_empty_file(filepath):
    """Create an empty file if it doesn't exist."""
    try:
        with open(filepath, "w") as f:
            pass
        logging.info(f"Creating empty file: {filepath}")
    except OSError as e:
        logging.error(f"Error creating empty file: {e}")

def create_directory_if_not_exists(directory):
    """Create a directory if it doesn't exist."""
    try:
        os.makedirs(directory, exist_ok=True)
        logging.info(f"Creating directory: {directory}")
    except OSError as e:
        logging.error(f"Error creating directory: {e}")
